var express = require("express");
var router = express.Router();
var axios = require("axios");

require("dotenv").config();

/**
 * Get token with axios
 */
const getToken = (() => {
  let timeStop;
  let token;

  return () => {
    if (timeStop && timeStop > new Date().getTime()) {
      return token;
    }

    const url = "https://svc.island.cognive.com";

    return axios
      .post(
        `${url}/auth/realms/spring-data-rest/protocol/openid-connect/token`,
        `grant_type=password&client_id=client&username=${process.env.LOGIN}&password=${process.env.PASSWORD}`,
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
        }
      )
      .then((response) => {
        timeStop = new Date().getTime() + response.data.expires_in * 1000;
        token = response.data.access_token;
        return response.data.access_token;
      })
      .catch((error) => {
        console.log(error.message);
      });
  };
})();

router.get("/", async function (req, res, next) {
  const token = await getToken();
  res.json({ title: "Express", token });
});

router.get("/search", async function (req, res, next) {
  const token = await getToken();

  const {
    clientId,
    fullname,
    dboId,
    passportNumber,
    inn,
    accountNumber,
    cardNumber,
  } = req.query;

  const parseQueryData = () => {
    const params = {};
    if (clientId) {
      params.exClientId = clientId;
    }
    if (fullname) {
      params.fullName = fullname;
    }

    if (dboId) {
      params.clientRbsList.dboClientId = dboId;
    }

    if (passportNumber) {
      params.verificationDocumentList.docNumber = passportNumber;
    }

    if (inn) {
      params.inn = inn;
    }

    if (accountNumber) {
      params.clientAccountList.accountnumber = accountNumber;
    }

    if (cardNumber) {
      params.clientCardsList.pk = cardNumber;
    }

    return params;
  };

  const stringDateToString = (date) => {
    if (!date) return "-";
    const dateParsed = new Date(date);
    return `${dateParsed.getDate()}.${dateParsed.getMonth()}.${dateParsed.getFullYear()}`;
  };

  const addressToLine = (item) => {
    if (!item || !item.addressList) return "-";
    const addres = item.addressList[0];
    return addres ? addres.addressLine : "-";
  };

  const mapResults = (data) => {
    return data.content.map((item) => {
      return {
        id: item.id,
        clientId: item.exClientId,
        fullName: item.fullName,
        birthdate: stringDateToString(item?.clientIndividual?.birthdate),
        addressLine: addressToLine(item),
        verificationDocument: "-",
      };
    });
  };

  axios
    .get("https://app1.island.cognive.com/api/clients", {
      params: {
        projection: "clientAfSearch",
        page: 0,
        size: 10,
        "clientType.code": 4,
        ...parseQueryData(),
      },
      headers: {
        authorization: `Bearer ${token}`,
      },
    })
    .then((response) => {
      res.json(mapResults(response.data));
    })
    .catch((error) => {
      res.json({ error: error.message });
    });
});

router.get("/client/:id", async function (req, res, next) {
  const token = await getToken();

  axios
    .get(`https://app1.island.cognive.com/api/clients/${req.params.id}`, {
      params: {
        projection: "clientAfDetails",
      },
      headers: {
        authorization: `Bearer ${token}`,
      },
    })
    .then((response) => {
      res.json(response.data);
    })
    .catch((error) => {
      res.json({ error: error.message });
    });
});

router.get("/clientProducts/:id", async function (req, res, next) {
  const token = await getToken();

  axios
    .get(`https://app1.island.cognive.com/api/clients/${req.params.id}`, {
      params: {
        projection: "clientAfDetails",
      },
      headers: {
        authorization: `Bearer ${token}`,
      },
    })
    .then((response) => {
      res.json(response.data.clientAccountList);
    })
    .catch((error) => {
      res.json({ error: error.message });
    });
});

router.get("/clientCards/:id", async function (req, res, next) {
  const token = await getToken();

  axios
    .get(`https://app1.island.cognive.com/api/clients/${req.params.id}`, {
      params: {
        projection: "clientAfDetails",
      },
      headers: {
        authorization: `Bearer ${token}`,
      },
    })
    .then((response) => {
      res.json(response.data.clientCardsList);
    })
    .catch((error) => {
      res.json({ error: error.message });
    });
});

module.exports = router;
